import React from 'react';
// @ts-ignore
import Background from './assets/images/background.png';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

// Own made components

import HeaderText from './components/headerText';
import InfoBox from './components/infoBox';
import SpillViewList from './components/spillViewList';
import MainBox from './components/mainBox';

const App = () => {
  return (
    <ScrollView>
      <ImageBackground style={{flex: 1}} source={Background}>
        <View style={{display: 'flex', flexDirection: 'column'}}>
          <View style={[styles.container, {paddingTop: '10%'}]}>
            {/* Using my component to create the header for the site */}
            <HeaderText text={'Nye Lotto-millionærer hver lørdag'}></HeaderText>
            {/** Use of infobox and state to update text continously */}
            <InfoBox />
          </View>
          <View style={styles.container}>
            <Text style={styles.text}>Spillefrist lørdag kl 18:00</Text>
          </View>
          {/** Using component mainbox to create a small or large box */}
          <View style={styles.sideContainer}>
            <MainBox selectedBox={'large'} text={'1 Uke'}></MainBox>
            <MainBox selectedBox={'large'} text={'5 Uker'}></MainBox>
            <MainBox selectedBox={'small'} text={'Abonner'}></MainBox>
            <MainBox selectedBox={'small'} text={'Fyll ut selv'}></MainBox>
          </View>
        </View>

        {/** Using component spillViewList to create number of elements */}
        <SpillViewList
          data={{
            headerText: 'Vinnersjanse 1 per nesten ingenting (14 millioner)',
            items: [
              'Lykkekupong',
              'Systemspill',
              'Slik spiller du',
              'Andelsbank',
              'Supertrekning',
            ],
            resultHeaderText: 'Resultater',
          }}></SpillViewList>
      </ImageBackground>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  //containters

  container: {
    flex: 1,
    padding: 10,
  },

  sideContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  //Text

  text: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Ubuntu',
    color: 'white',
    padding: 10,
  },
});

export default App;
