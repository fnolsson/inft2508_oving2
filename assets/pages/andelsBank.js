import React from 'react';
import {View, Text, ImageBackground} from 'react-native';
import Background from '../images/background.png';

const AndelsBank = () => {
  return (
    <ImageBackground style={{flex: 1}} source={Background}>
      <View>
        <Text
          style={{
            fontFamily: 'Ubuntu',
            fontSize: 25,
            color: 'white',
            textAlign: 'center',
            textAlignVertical: 'center',
            paddingTop: 50,
          }}>
          AndelsBank spel
        </Text>
      </View>
    </ImageBackground>
  );
};
export default AndelsBank;
