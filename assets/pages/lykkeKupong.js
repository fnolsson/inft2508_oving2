import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  SectionList,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Button,
} from 'react-native';
import Background from '../images/background.png';

//Add Api support for fetching and updating numbers

//Setting up local IP since there is sometimes a shift in address
const localIp = 'http://192.168.56.1:3000/data';

const LykkeKupong = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [newEntry, setNewEntry] = useState({
    spill: '',
    name: '',
    num: [],
  });
  const [editingId, setEditingId] = useState(null);

  //UseEffect to fetch data

  useEffect(() => {
    const fetchData = async () => {
      try {
        //Have also tried to use ngrok and it works fine as long as server is up and running locally
        let response = await fetch(localIp); // Using local ip to make it work with android emulator
        let jsonData = await response.json();
        setData(jsonData);
      } catch (error) {
        console.error('Fetch error: ', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  //Use to post data and create new  input
  const createData = async newData => {
    try {
      let response = await fetch(localIp, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newData),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      let jsonResponse = await response.json();
      setData([...data, jsonResponse]);
    } catch (error) {
      console.error('POST error:', error);
    }
  };
  // updating existing data
  const updateData = async (id, updatedData) => {
    try {
      let response = await fetch(localIp + '/' + {id}, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedData),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      let jsonResponse = await response.json();

      let updatedItems = data.map(item =>
        item.id === id ? jsonResponse : item,
      );

      setData(updatedItems);
    } catch (error) {
      console.error('PUT error:', error);
    }
  };

  // delete existing data
  const deleteData = async id => {
    try {
      let response = await fetch(localIp + '/' + {id}, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      setData(data.filter(item => item.id !== id));
    } catch (error) {
      console.error('DELETE Error:', error);
    }
  };
  const handleAdd = () => {
    if (newEntry.spill && newEntry.name && newEntry.num.length > 0) {
      createData(newEntry);
      setNewEntry({spill: '', name: '', num: []});
    } else {
      alert('Alle felt må være ifyllt');
    }
  };
  const handleEdit = item => {
    setNewEntry(item);
    setEditingId(item.id);
  };

  const handleSave = () => {
    const numArray = newEntry.num.split(',').map(n => parseInt(n.trim()));

    if (newEntry.spill && newEntry.name && numArray.length > 0) {
      const updatedEntry = {
        ...newEntry,
        num: numArray,
      };

      if (editingId) {
        updateData(editingId, updatedEntry);
      } else {
        createData(updatedEntry);
      }

      setNewEntry({spill: '', name: '', num: ''});
    } else {
      alert('Alle felt må være ifyllt');
    }
  };

  if (isLoading) {
    return <Text>Loading...</Text>;
  }

  if (!data) {
    return <Text>No data available</Text>;
  }

  //Group data after its respective game
  const groupedData = Array.from(
    data
      .reduce((acc, item) => {
        if (!acc.has(item.spill)) {
          acc.set(item.spill, {title: item.spill, data: []});
        }
        acc.get(item.spill).data.push(item);
        return acc;
      }, new Map())
      .values(),
  );

  return (
    <ImageBackground source={Background} style={styles.background}>
      <Text style={styles.header}>LykkeKupong</Text>
      <View>
        <TextInput
          style={styles.input}
          placeholder="Type spill (Lotto, Viking...)"
          value={newEntry.spill}
          onChangeText={text => setNewEntry(prev => ({...prev, spill: text}))}
        />
        <TextInput
          style={styles.input}
          placeholder="Navn"
          value={newEntry.name}
          onChangeText={text => setNewEntry(prev => ({...prev, name: text}))}
        />
        <TextInput
          keyboardType="numeric"
          style={styles.input}
          placeholder="Dine nummer med ' , ' tegn mellom "
          value={newEntry.num}
          onChangeText={text =>
            setNewEntry(prev => ({
              ...prev,
              num: text,
            }))
          }
        />

        <View style={styles.button}>
          <Button
            title={editingId ? 'Spare endringer' : 'Legg til '}
            onPress={handleSave}
          />
        </View>
      </View>
      <View style={styles.container}>
        <SectionList
          sections={groupedData}
          renderItem={({item}) => (
            <TouchableOpacity style={styles.listItemContainer}>
              <Text style={styles.name}>{item.name}</Text>
              <Text style={styles.item}>
                {Array.isArray(item.num) ? item.num.join(',  ') : ''}
              </Text>

              <View style={styles.button}>
                <Button title="Endre" onPress={() => handleEdit(item)} />
                <Button title="Slett" onPress={() => deleteData(item.id)} />
              </View>
            </TouchableOpacity>
          )}
          renderSectionHeader={({section}) => (
            <Text style={styles.sectionHeader}>{section.title}</Text>
          )}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  button: {flexDirection: 'row', justifyContent: 'space-around'},
  header: {
    fontFamily: 'Ubuntu',
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingTop: 50,
  },
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: 40,
  },
  sectionHeader: {
    padding: 10,
    fontSize: 20,
    backgroundColor: 'rgba(0,0,0,0.3)',
    fontWeight: 'bold',
    color: 'white',
  },
  listItemContainer: {
    padding: 10,
    borderBottomWidth: 0.7,
    borderBottomColor: 'rgba(0,0,0,0.5)',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  name: {
    textAlign: 'center',
    fontSize: 18,
    color: 'lightgrey',
  },
  item: {
    textAlign: 'center',
    fontSize: 15,
    color: 'white',
    padding: '4%',
  },
  input: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    margin: '1%',
  },
});

export default LykkeKupong;
