import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity} from 'react-native';
import Background from '../images/background.png';
import {useNavigation} from '@react-navigation/native';

const Route = () => {
  const navigation = useNavigation();
  const navigationPress = item => {
    switch (item) {
      case 'Systemspill':
        navigation.navigate('SystemSpill');
        break;

      case 'Andelsbank':
        navigation.navigate('AndelsBank');
        break;
      case 'Supertrekning':
        navigation.navigate('SuperTrekning');
        break;
      case 'Instillinger':
        navigation.navigate('SettingsScreen');
        break;
      default:
        navigation.navigate('Home');
    }
  };
  const data = {
    headerText: 'Alternativer',
    items: ['Systemspill', 'Andelsbank', 'Supertrekning', 'Instillinger'],
  };
  return (
    <ImageBackground style={{flex: 1}} source={Background}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.2)',
          width: '100%',
          paddingTop: 30,
          marginTop: '15%',
          marginBottom: '30%',
          flexDirection: 'column',
          flexWrap: 'wrap',
          alignItems: 'center',
        }}>
        <View style={{width: '100%'}}>
          <Text
            style={{
              fontSize: 12,
              padding: 5,
              textAlign: 'center',
              color: 'black',
            }}>
            {data.headerText}
          </Text>
        </View>
        {data.items.map((item, index) => (
          <TouchableOpacity
            key={index}
            style={[
              {
                backgroundColor: 'lightgrey',
                borderRadius: 5,
                flex: 1,
                margin: 10,
                paddingTop: 15,
                paddingBottom: 15,
                minWidth: '40%',
                maxWidth: '60%',
                maxHeight: '15%',
                justifyContent: 'center',
              },
            ]}
            onPress={() => navigationPress(item)}>
            <View>
              <Text
                style={{
                  color: 'black',
                  fontFamily: 'Ubuntu',
                  textAlign: 'center',
                  fontSize: 13,
                }}>
                {item}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </ImageBackground>
  );
};
export default Route;
