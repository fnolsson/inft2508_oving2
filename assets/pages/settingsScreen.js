import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  Button,
  Alert,
  StyleSheet,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {useGame} from '../../components/gameContext';
import Background from '../images/background.png';

const SettingsScreen = () => {
  const {gameName, setGameName} = useGame();
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    fetchStoredGameName();
  }, []);

  const fetchStoredGameName = async () => {
    try {
      const storedName = await AsyncStorage.getItem('gameName');
      if (storedName) {
        setGameName(storedName);
      }
    } catch (error) {
      Alert.alert('Error', 'Noe gikk galt da vi prøvde å hente spillet');
    }
  };

  const handleSaveGameName = async () => {
    try {
      await AsyncStorage.setItem('gameName', inputValue);

      setGameName(inputValue);
      setInputValue('');
    } catch (error) {
      Alert.alert('Error', 'Noe gikk galt');
    }
  };

  return (
    <ImageBackground source={Background} style={styles.container}>
      <View>
        <Text style={styles.headerText}>Legg inn et nytt spill</Text>
        <TextInput
          style={styles.input}
          value={inputValue}
          onChangeText={setInputValue}
          placeholder="Navnet på spillet eller meldingen"
          placeholderTextColor="white"
        />
        <Button title="Spare" onPress={handleSaveGameName} />
        <Text style={styles.headerText}>Siste sparte spillet:</Text>
        <Text style={styles.headerText}>{gameName}</Text>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: 'Ubuntu',
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingBottom: 20,
    paddingTop: 20,
  },
  input: {
    height: 40,
    width: 250,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 4,
    color: 'white',
    paddingHorizontal: 10,
    marginBottom: 20,
  },
});

export default SettingsScreen;
