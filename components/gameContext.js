import React, {createContext, useState, useContext} from 'react';

const GameContext = createContext();

export const GameProvider = ({children}) => {
  const [gameName, setGameName] = useState('');

  return (
    <GameContext.Provider value={{gameName, setGameName}}>
      {children}
    </GameContext.Provider>
  );
};

export const useGame = () => {
  const context = useContext(GameContext);
  if (!context) {
    throw new Error(Error.name);
  }
  return context;
};
