import React from 'react';
import {Text, View} from 'react-native';

const HeaderText = ({text}) => {
  return (
    <View style={{padding: 15}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 25,
          fontFamily: 'Ubuntu',
          color: 'white',
        }}>
        {text}
      </Text>
    </View>
  );
};

export default HeaderText;
