import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const textOptions = [
  'Lotto premie nå kr 1 000 000',
  'Spill med ansvar',
  'Eurojackpot premie tirsdag 350 millioner',
  'Støtt din lokale forening med grasrotandel',
  'Ny skraplott ute i butikk - Høstgullregn',
];

const InfoBox = () => {
  const [infoText, setInfoText] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      // Henter spillet som vi sparte på instillinger
      const gameName = await AsyncStorage.getItem('gameName');

      if (gameName) {
        // Om det eksisterer et navn så legger vi det til med en gang
        setInfoText(gameName);

        let index = 0;
        const int = setInterval(() => {
          index = (index + 1) % (textOptions.length + 1);
          if (index === 0) {
            setInfoText(gameName);
          } else {
            setInfoText(textOptions[index - 1]);
          }
        }, 15000);

        return () => {
          clearInterval(int);
        };
      }
    };

    fetchData();
  }, []);

  return (
    <View
      style={{
        backgroundColor: 'rgba(0,0,0,.3)',
        borderRadius: 30,
        maxWidth: '90%',
        alignSelf: 'center',
        paddingHorizontal: 15,
        paddingVertical: 6,
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontFamily: 'Ubuntu Light',
          fontSize: 13,
          color: 'white',
        }}>
        {infoText}
      </Text>
    </View>
  );
};

export default InfoBox;
