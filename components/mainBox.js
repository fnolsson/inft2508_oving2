import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const MainBox = ({selectedBox, text}) => {
  const navigation = useNavigation();
  const handlePress = () => {
    navigation.navigate('NyttSpill');
  };
  return (
    <TouchableOpacity onPress={handlePress}>
      <View
        style={[selectedBox === 'large' ? styles.largeBox : styles.smallBox]}>
        <Text
          style={{
            color: '#D41040',
            fontFamily: 'Ubuntu',
            textAlign: 'center',
          }}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = {
  largeBox: {
    backgroundColor: 'white',
    borderRadius: 5,
    flex: 1,
    margin: 10,
    paddingTop: 20,
    paddingBottom: 20,
    minWidth: '40%',
  },

  smallBox: {
    backgroundColor: 'white',
    borderRadius: 5,
    flex: 1,
    margin: 10,
    marginTop: 5,
    height: 40,
    minWidth: '40%',
    justifyContent: 'center',
  },
};

export default MainBox;
