import React from 'react';
import {View, Text, TouchableOpacity, Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const SpillViewList = ({data}) => {
  const navigation = useNavigation();
  const handlePress = () => {
    // Display a prompt when the MainBox is pressed
    Alert.alert(
      'Resultater',
      '1a Premie til en mann i Oslo, 1.7 Millioner',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            // Handle the OK action here
            console.log('OK pressed');
          },
        },
      ],
      {cancelable: false},
    );
  };
  const navigationPress = item => {
    switch (item) {
      case 'Lykkekupong':
        navigation.navigate('LykkeKupong');
        break;
      case 'Systemspill':
        navigation.navigate('SystemSpill');
        break;
      case 'Slik spiller du':
        navigation.navigate('SlikSpillerDu');
        break;
      case 'Andelsbank':
        navigation.navigate('AndelsBank');
        break;
      case 'Supertrekning':
        navigation.navigate('SuperTrekning');
        break;
      default:
        navigation.navigate('Home');
    }
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        height: 'auto',
        marginTop: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
      }}>
      <View style={{width: '100%'}}>
        <Text
          style={{
            fontSize: 12,
            padding: 5,
            textAlign: 'center',
            color: 'black',
          }}>
          {data.headerText}
        </Text>
      </View>
      {data.items.map((item, index) => (
        <TouchableOpacity
          key={index}
          style={[
            {
              backgroundColor: 'lightgrey',
              borderRadius: 5,
              flex: 1,
              margin: 10,
              paddingTop: 15,
              paddingBottom: 15,
              minWidth: '40%',
              maxWidth: '41%',
            },
          ]}
          onPress={() => navigationPress(item)}>
          <View>
            <Text
              style={{
                color: 'black',
                fontFamily: 'Ubuntu',
                textAlign: 'center',
                fontSize: 13,
              }}>
              {item}
            </Text>
          </View>
        </TouchableOpacity>
      ))}
      <TouchableOpacity onPress={handlePress}>
        <View
          style={{
            backgroundColor: '#D41040',
            borderRadius: 5,
            flex: 1,
            margin: 10,
            paddingTop: 15,
            paddingBottom: 15,
            minWidth: '80%',
          }}>
          <Text
            style={{
              color: 'white',
              fontFamily: 'Ubuntu',
              paddingLeft: 10,
              fontSize: 17,
            }}>
            {data.resultHeaderText}
          </Text>
          <Text
            style={{
              color: 'white',
              fontFamily: 'Ubuntu Light',
              paddingLeft: 10,
            }}>
            Alle Vinnere og premier
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default SpillViewList;
