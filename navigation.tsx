import 'react-native-gesture-handler';
import React from 'react';
import {
  DrawerActions,
  NavigationContainer,
  useNavigation,
} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import App from './App';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LykkeKupong from './assets/pages/lykkeKupong';
import AndelsBank from './assets/pages/andelsBank';
import SlikSpillerDu from './assets/pages/slikSpillerDu';
import SuperTrekning from './assets/pages/superTrekning';
import SystemSpill from './assets/pages/systemSpill';
import NyttSpill from './assets/pages/nyttSpill';
import Icon from 'react-native-vector-icons/Ionicons';
import Route from './assets/pages/route';
import SettingsScreen from './assets/pages/settingsScreen';
import {GameProvider} from './components/gameContext';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function StackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Route">
      <Stack.Screen
        name="Route"
        component={Route}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{headerTransparent: true, headerTitle: ''}}
      />
      <Stack.Screen
        name="SuperTrekning"
        component={SuperTrekning}
        options={{headerTransparent: true, headerTitle: ''}}
      />
      <Stack.Screen
        name="SystemSpill"
        component={SystemSpill}
        options={{headerTransparent: true, headerTitle: ''}}
      />
      <Stack.Screen
        name="AndelsBank"
        component={AndelsBank}
        options={{
          headerTransparent: true,
          headerTitle: '',
        }}
      />
    </Stack.Navigator>
  );
}

function TabNavigator() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'LykkeKupong') {
            iconName = focused ? 'heart' : 'heart-outline';
          } else if (route.name === 'SlikSpillerDu') {
            iconName = focused
              ? 'caret-forward-circle'
              : 'caret-forward-circle-outline';
          } else if (route.name === 'NyttSpill') {
            iconName = focused ? 'add-circle' : 'add-circle-outline';
          } else if (route.name === 'Mer') {
            iconName = focused ? 'menu' : 'menu-outline';
          }
          //@ts-ignore
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}>
      <Tab.Screen name="Home" component={App} options={{headerShown: false}} />
      <Tab.Screen
        name="LykkeKupong"
        component={LykkeKupong}
        options={{headerShown: false}}
      />

      <Tab.Screen
        name="SlikSpillerDu"
        component={SlikSpillerDu}
        options={{headerShown: false}}
      />

      <Tab.Screen
        name="NyttSpill"
        component={NyttSpill}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Mer"
        component={StackNavigator}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
}

const Navigation = () => {
  return (
    <GameProvider>
      <NavigationContainer>
        <TabNavigator />
      </NavigationContainer>
    </GameProvider>
  );
};

export default Navigation;
